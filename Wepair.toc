## Interface: 70300
## Title: Wepair
## Notes: Allows a Guild Master to swap between several guild bank repair setups
## Author: TheNerdWonder
## LoadOnDemand: 1
## LoadWith: Blizzard_GuildUI
## SavedVariables: Wepair_Profiles

Wepair.xml