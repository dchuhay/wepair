Wepair allows Guild Masters in World of Warcraft to set maximum guild repair allowances for each rank with a single command

# Slash Commands #
* **/list** - Displays a list of all the known profiles
* **/display [name]** - Prints the maximum guild repair for each level for the named profile
* **/create [name]** - Creates a new profile with the provided name
* **/update [name] value_1 value_2 ...** - Updates the maximum repair values for the named profile with the provided values
* **/setactive [name]** - Sets the Guild Repair limits using the values associated with the named profile