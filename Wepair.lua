local _, Wepair = ...

local function Wepair_split(str)
	local t = {}
	for token in string.gmatch(str, '[^%s]+') do
		table.insert(t, token)
	end
	
	return t
end

local function Wepair_UpdateProfile(profileName, values)
	local profile = Wepair_Profiles[profileName]
	local profileSize = #profile
	local valuesSize = #values
	
	if profileSize == valuesSize - 1 then
		for i=1, profileSize, 1 do
			profile[i] = values[i+1]
		end
		
		print("Profile '" .. profileName .. "' has been updated.")
	else
		print("Cannot update profile '" .. profileName .. "'. Expected " .. profileSize .. " values but found " .. (valuesSize - 1) .. ".")
	end
end

local function Wepair_UpdateRankLimits(profile)
	for i, amount in ipairs(profile) do
		local rankName = Wepair.RankNames[i]
--		print('setting limit for' .. rankName .. ' to '.. amount)
		GuildControlSetRank(i)
		SetGuildBankWithdrawGoldLimit(profile[i])		
		GuildControlSaveRank(rankName)
	end
end

local function Wepair_InitProfile(profileName)
	local rankCount = GuildControlGetNumRanks()
	
	local profile = {}
	for i = 1, rankCount, 1 do
		profile[i] = 0
	end
	
	Wepair_Profiles[profileName] = profile
end

local function Wepair_HandleRankUpdate()
	local profiles = Wepair_Profiles
	
	-- Don't handle updates on rank init. The game's not ready yet.
	if profiles == nil then
		return
	end
	
	local newCount = GuildControlGetNumRanks()
	local oldCount = #(profiles['None'])
	
--	print('old: ' .. oldCount .. ' new: ' .. newCount)
	
	if oldCount < newCount then			-- New rank added to end of list. Update accordingly
--		print('Handling new rank...')
		local newRankName = GuildControlGetRankName(newCount)
		table.insert(Wepair.RankNames, newRankName)

		for k,v in pairs(Wepair_Profiles) do
			table.insert(v, 0)
		end
	elseif oldCount > newCount then			-- Rank removed. Figure out which one and update accordingly
--		print('Handling removed rank...')
		for i=1, oldCount, 1 do
			local newRankName = GuildControlGetRankName(i)
			local oldRankName = Wepair.RankNames[i]
			if not (newRankName == oldRankName) then
				table.remove(Wepair.RankNames, i)

				for k,v in pairs(Wepair_Profiles) do
					table.remove(v, i)
				end
				
				return
			end
		end
	elseif oldCount == newCount then	-- Ranks swapped. Figure out which ones and update accordingly
--		print('Handling rank swap...')
		for i=1, oldCount, 1 do
			local newRankName = GuildControlGetRankName(i)
			local oldRankName = Wepair.RankNames[i]
--			print(i .. ': ' .. oldRankName .. ' - ' .. newRankName)

			if not (newRankName == oldRankName) then
--				print('Difference found at index ' .. i)
				local temp = Wepair.RankNames[i]
				Wepair.RankNames[i] = Wepair.RankNames[i+1]
				Wepair.RankNames[i+1] = temp

				for k,v in pairs(Wepair_Profiles) do
					local temp = v[i]
					v[i] = v[i+1]
					v[i+1] = temp
				end

				return
			end
		end
	end
	
end

SLASH_WEPAIR1 = '/wepair'
SlashCmdList["WEPAIR"] = function(msg, editBox)
	local profiles = Wepair_Profiles
	local command, rest = msg:match('^(%S*)%s*(.-)$')
	
	if profiles == nil then
		print('Wepair functions cannot be used before the Guild information has been loaded. Please open the Guild window and try again.')
		return
	end
	
	if command == 'list' then
		print('Defined Wepair profiles:')
		for k,v in pairs(Wepair_Profiles) do
			print(' - ' .. k)
		end
	elseif command == 'display' then
		if profiles[rest] == nil then
			print("Profile '" .. rest .. "' does not exist.")
		else
			print(rest)
			for i,v in ipairs(profiles[rest]) do
				print('  ' .. Wepair.RankNames[i] .. ': ' .. v)
			end
		end
	elseif command == 'create' then
		if profiles[rest] == nil then
			Wepair_InitProfile(rest)
			print("Profile '" .. rest .. "' has been created.")
		else
			print("Profile '" .. rest .. "' already exists.")
		end
	elseif command == 'update' then
		local tokens = Wepair_split(rest)
		local profileName = tokens[1]
		if profiles[profileName] == nil then
			print("Profile '" .. rest .. "' does not exist.")
		else
			Wepair_UpdateProfile(profileName, tokens)
		end
	elseif command == 'delete' then
		local tokens = Wepair_split(rest)
		local profileName = tokens[1]
		if profiles[profileName] == nil then
			print("Profile '" .. rest .. "' does not exist.")
		else
			profiles[profileName] = nil
			print("Profile '" .. rest .. "' has been deleted.")
		end
	elseif command == 'setactive' then
		if profiles[rest] == nil then
			print("Profile '" .. rest .. "' does not exist.")
		else
			local _, _, guildRankIndex, _ = GetGuildInfo('player')
			if guildRankIndex == 0 then
				print('Updating guild repair limits to profile ' .. rest .. '...')
				Wepair_UpdateRankLimits(profiles[rest])
				print('Done.')
			else
				print('This function is reserved for Guild Masters.')
			end
		end
	else
		if command == nil then
			print("Unknown command '" .. rest .. "'")
		end
		print('Valid Wepair commands:')
		print('  list: Displays a list of all the known profiles')
		print('  display [name]: Print the maximum Guild Repair for the named profile')
		print('  create [name]: Creates a new profile with the provided name')
		print('  update [name] value_1 value_2 ... : Updates the maximum repair values for the named profile with the provided values (highest rank to lowest)')
		print('  delete [name]: Deletes the named profile')
		print('  setactive [name]: Sets the Guild Repair limits using the named profile')
	end
end

local frame = CreateFrame('Frame')
frame:RegisterEvent('ADDON_LOADED')
frame:RegisterEvent('GUILD_ROSTER_UPDATE')
frame:RegisterEvent('GUILD_RANKS_UPDATE')

frame:SetScript('OnEvent', function(self, event, arg1)
	if event == 'ADDON_LOADED' and arg1 == 'Wepair' then
		local rankCount = GuildControlGetNumRanks()
		Wepair.RankNames = {}
		
		for i=1, rankCount, 1 do
			local rankName = GuildControlGetRankName(i)
			table.insert(Wepair.RankNames, rankName)
		end
		
		print('Wepair has been loaded.')
    elseif event == 'GUILD_ROSTER_UPDATE' then
        -- Our saved variables, if they exist, have been loaded at this point.
        if Wepair_Profiles == nil then
			Wepair_Profiles = {}
			Wepair_InitProfile('None')
        end
	elseif event == 'GUILD_RANKS_UPDATE' then
		Wepair_HandleRankUpdate()
	end
end)
